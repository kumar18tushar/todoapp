import React, { Component } from 'react';
import { render } from 'react-dom';
import style from './css/style.css'
import TodoListView from './TodoListView.js'



class Box extends Component
{
    constructor(){
        super();
        
        this.state={
            todos:[]
        }
        this.additem = this.additem.bind(this);
        this.clearall = this.clearall.bind(this);
    }

    additem(e){
        e.preventDefault();
        
        let td=this._inputElement.value;
        let prev=this.state.todos;
        prev.push(td);

        this.setState({
            todos:prev,
        });

        console.log(this.state.todos)
    }


    clearall(e){
        e.preventDefault();
        this.setState({
            todos:[],
        })
    }

    render(){
        return(
            <div>
                <div className="formsection">
                    <span className="in">
                    <form onSubmit={this.additem}> 
                        Add a Todo :  <input type="text" ref={(a) => this._inputElement = a}></input>     
                        <input type="submit" value="Submit" ></input>
                    </form>

                    <button type="Delete all" value="delete all" className="del" onClick={this.clearall}>Clear All</button>
                    </span>
                </div>

                 <TodoListView todos={this.state.todos}/> 
                
            </div>
    );};
}

export default Box;

