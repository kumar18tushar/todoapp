import React, { Component } from 'react';
import './App.css';
import Title from './appcomponent/Title'
import Box from './appcomponent/Box'
import TodoListView from './appcomponent/TodoListView'
import style from './appcomponent/css/style.css'

class App extends Component {
  render() {
    return (
      <div>
        <div  className="content">
            <Title/>
        </div>

        <div className="boxpart">
            <Box/> 
        </div>
        </div>

    );
  }
}

export default App;
